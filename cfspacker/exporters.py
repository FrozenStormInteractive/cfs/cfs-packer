import struct
import json
import uuid
from cfspacker.structs import Index

def pack_string(s):
    data = bytearray()
    if s is not None and len(s) > 0:
        data.extend(struct.pack('H', len(s)))
        data.extend(s.encode())
    else:
        data.extend(struct.pack('H', 0))
    return data

def pack_guid(guid):
    return uuid.UUID(guid).bytes

class BinaryExporter:

    def __init__(self, index):
        self.index = index

    def export(self):

        data = bytearray()

        data.extend([ord('C'), ord('F'), ord('S')])
        data.extend(struct.pack('B', 1))

        data.extend(pack_string(self.index.root))

        data.extend(struct.pack('I', len(self.index.containers)))
        data.extend(struct.pack('I', len(self.index.entries)))

        for cont in self.index.containers:
            data.extend(pack_guid(cont.guid))
            data.extend(pack_string(cont.path))

        for entry in self.index.entries:
            data.extend(pack_guid(entry.guid))
            data.extend(pack_string(entry.path))

            if entry.hash is not None:
                md5hash = entry.hash.md5
                sha1hash = entry.hash.sha1
            else:
                md5hash = None
                sha1hash = None

            data.extend(pack_string(md5hash))
            data.extend(pack_string(sha1hash))

            data.extend(struct.pack('I', len(entry.blocks)))

            for block in entry.blocks:
                data.extend(block.container.encode())
                data.extend(struct.pack('I', block.offset))
                data.extend(struct.pack('I', block.size))

        return data


def convert_to_dict(obj):
    return obj.__dict__


class JsonExporter:
    def __init__(self, index, compression=True):
        self.index = index
        self.compression = compression

    def export(self):
        if self.compression:
            return json.dumps(self.index, separators=(',', ':'), default=convert_to_dict)
        else:
            return json.dumps(self.index, indent=4, default=convert_to_dict)
