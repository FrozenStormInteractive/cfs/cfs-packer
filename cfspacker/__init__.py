import argparse
import sys
import os
import uuid
import json
import hashlib
import math
from glob import glob
import cfspacker.generator
from cfspacker.exporters import BinaryExporter, JsonExporter
from cfspacker.structs import *
from cfspacker.importers import JsonImporter

def main():
    parser = argparse.ArgumentParser()

    parser.add_argument('-o','--out', metavar='output', type=str)
    parser.add_argument('-b', '--binary', help="Create binary index instead of json index", action="store_true")
    parser.add_argument('--max-pak-size', metavar='s', type=int, default=200)
    parser.add_argument('--max-chunk-size', metavar='s', type=int, default=200)
    parser.add_argument("--no-integrity-check", help="Disable integrity check", action="store_true")
    parser.add_argument("--no-index-compression", help="Disable index compression", action="store_true")
    parser.add_argument('-e', '--extends', help="Extends index if exists in output directory", action="store_true")
    parser.add_argument('files', metavar='file', nargs='+')

    args = parser.parse_args()

    if args.max_pak_size < args.max_chunk_size:
        print("Chunk size cannot be larger than pak size", file=sys.stderr)
        sys.exit(1)
        return

    max_chunk_size = args.max_chunk_size * 1024 * 1024
    max_pak_size = args.max_pak_size * 1024 * 1024

    if args.binary:
        index_filename = "index.pakindex"
        if args.out is not None:
            index_filename = os.path.join(args.out, index_filename)

        exporter_class = BinaryExporter
        importer_class = None
    else:
        index_filename = "index.json"

        if args.out is not None:
            index_filename = os.path.join(args.out, index_filename)

        exporter_class = JsonExporter
        importer_class = JsonImporter

    index = None
    if os.path.isfile(index_filename) and importer_class is not None:
        with open(index_filename, 'r') as outfile:
            importer = importer_class(outfile.read())
            index = importer.imports()

    files = []
    for file in args.files:
        files += glob(file, recursive=True)

    if args.out is not None:
        os.makedirs(args.out, exist_ok=True)

    index = generator.generate(files, args.out, index, max_pak_size, max_chunk_size, not args.no_integrity_check)

    exporter = exporter_class(index, not args.no_index_compression)
    if args.binary:
        with open(index_filename, 'wb') as outfile:
            outfile.write(exporter.export())
    else:
        with open(index_filename, 'w') as outfile:
            outfile.write(exporter.export())
