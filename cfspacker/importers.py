import os, json
from jsonschema import validate
from cfspacker.structs import Index, Container, Entry, Hash, Block

class JsonImporter:
    def __init__(self, data):
        self.data = data

    def imports(self):
        obj = json.loads(self.data)

        with open(os.path.join(os.path.dirname(__file__), 'schemas', 'index.schema.json'), 'r') as schema_file:
            schema = json.loads(schema_file.read())

        validate(instance=obj, schema=schema)

        containers = []
        for container_data in obj["containers"]:
            container = Container(container_data["guid"], container_data["path"])
            containers.append(container)

        entries = []
        for entry_data in obj["entries"]:
            entry = Entry(entry_data["guid"], entry_data["path"])

            if "hash" in entry_data:
                entry.hash = Hash(entry_data["hash"]["md5"], entry_data["hash"]["sha1"])

            for block_data in entry_data["blocks"]:
                entry.blocks.append(Block(block_data["container"], block_data["offset"], block_data["size"]))

            entries.append(entry)

        return Index(obj["root"], containers, entries)
