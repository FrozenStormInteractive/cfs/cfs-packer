import os
import math
import uuid
import hashlib
import cfspacker.default as default
from cfspacker.structs import *

def prepare_paks(files, max_chunk_size, max_pak_size):
    paks = []

    def append_chunk(source, offset, size):
        c = {
            "source": source,
            "offset": offset,
            "size": size,
        }

        p = None
        for pak in paks:
            if pak["size"] + c["size"] <= max_pak_size:
                p = pak
                break

        if p is not None:
            p["size"] += c["size"]
            p["chunks"].append(c)
        else:
            paks.append({
                "size": c["size"],
                "chunks": [c]
            })

    for file in files:
        if not os.path.exists(file) or not os.path.isfile(file):
            continue

        fsize = os.path.getsize(file)

        if fsize <= max_chunk_size:
            append_chunk(file, 0, fsize)
        else:
            d = math.ceil(fsize / max_chunk_size)
            chunk_size = int(fsize / d)
            offset = 0
            for i in range(0, d - 1):
                append_chunk(file, offset, chunk_size)
                offset += chunk_size

            append_chunk(file, offset, fsize - offset)

    return paks

def generate(files, output_path = None, index = None, max_pak_size = default.MAX_CHUNK_SIZE,
             max_chunk_size = default.MAX_PAK_SIZE, integrity_check=True):

    containers = {}
    entries = {}

    root_path = ""
    if index is not None:
        root_path = index.root

        for container in index.containers:
            containers[container.guid] = container

        for entry in index.entries:
            entries[entry.path] = entry

        files = [file for file in files if os.path.basename(file) not in entries]

    paks = prepare_paks(files, max_chunk_size, max_pak_size)

    for pak in paks:
        cont_id = str(uuid.uuid1())

        container = Container(cont_id, cont_id + '.pak')
        containers[cont_id] = container

        if output_path is not None:
            path = os.path.join(output_path, container.path)
        else:
            path = container.path

        with open(path, 'wb') as outfile:
            for chunk in pak["chunks"]:
                with open(chunk["source"], "rb") as f:

                    if chunk["source"] in entries:
                        entry = entries[chunk["source"]]
                    else:
                        entry = Entry(str(uuid.uuid1()), os.path.basename(chunk["source"]))

                        if integrity_check:
                            md5hash = hashlib.md5()
                            sha1hash = hashlib.sha1()
                            f.seek(0, 0)
                            for filepart in iter(lambda: f.read(4096), b""):
                                md5hash.update(filepart)
                                sha1hash.update(filepart)

                            entry.hash = Hash(md5hash.hexdigest(), sha1hash.hexdigest())

                        entries[chunk["source"]] = entry

                    entry.blocks.append(Block(cont_id, outfile.tell(), chunk["size"]))

                    f.seek(chunk["offset"], 0)
                    outfile.write(f.read(chunk["size"]))

    return Index(root_path, list(containers.values()), list(entries.values()))

