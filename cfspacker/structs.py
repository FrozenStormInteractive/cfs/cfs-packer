
class Index:
    root = None
    containers = []
    entries = []

    def __init__(self, root, containers, entries):
        self.root = root
        self.containers = containers
        self.entries = entries


class Container:
    guid = None
    path = None

    def __init__(self, guid, path):
        self.guid = guid
        self.path = path


class Block:
    container = None
    offset = -1
    size = -1

    def __init__(self, container, offset, size):
        self.container = container
        self.offset = offset
        self.size = size


class Hash:
    md5 = None
    sha1 = None

    def __init__(self, md5, sha1):
        self.md5 = md5
        self.sha1 = sha1


class Entry:
    guid = None
    path = None
    blocks = []
    hash = None

    def __init__(self, guid, path):
        self.guid = guid
        self.path = path
        self.blocks = []
