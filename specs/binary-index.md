# Binary Index Format

## File Structure

```
Byte[3] identifier
Byte version
String root
UInt32 containerCount
UInt32 entryCount

for each container in containerCount
    Guid guid
    String path
end

for each entry in entryCount
    Guid guid
    String path
    String md5
    String sha1
    UInt32 blockCount

    for each block in blockCount
        Guid container
        UInt32 offset
        UInt32 size
    end
end
```

### String Structure

```
struct String {
  UInt32 length
  Byte characters[length]
}
```

### Guid Structure

```
struct Guid {
  Byte characters[16]
}
```

### Fields

### identifier

The file identifier is a unique set of bytes that will differentiate the file from other types of files. It consists of 3 bytes, as follows:

```c
Byte[3] FileIdentifier = {
  0x43, 0x46, 0x53
}
```

This can also be expressed using C-style character definitions as:

```c
Byte[3] FileIdentifier = {
  'C', 'F', 'S'
}
```

### version
